
import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import CryptoJS from 'crypto-js';
import SecretKey from '../../../../config/SecretKeys';

export default class Login extends Component {

    // componentDidMount() {
    //     const userData = localStorage.getItem('USER_DATA');
    //     // console.log("ENCRYPTED DATA FROM LOCAL STORAGE ==> ", userData);

    //     if (userData) {
    //         const decryptedUserData = this.decrypt(userData)
    //         // console.log("DECRYPTED USER DATA ==> ", decryptedUserData);

    //         if (!decryptedUserData.access_token) {
    //             console.log("decryptedUserData.access_token ==>> ", decryptedUserData.access_token);
    //             this.props.history.push('/admin/login')
    //         }

    //     } else {
    //         this.props.history.push('/admin/login')
    //     }
    // }

    // decrypt = (value) => {
    //     let bytes = CryptoJS.AES.decrypt(value, SecretKey);
    //     let decryptedUserData = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
    //     return decryptedUserData;
    // };

    handleNavigation = (e) => {
        this.props.history.push(e)
        // console.log(this.props)
    }

    render() {
        return (
            <div className="container-fluid vh-100 d-flex justify-content-center align-items-center">
                <div className="row shadow mx-1 my-3">
                    <span onClick={() => this.handleNavigation('/admin/dashboard')} className="nav-link">Go to dashboard</span>
                </div>
            </div>
        );
    }
}