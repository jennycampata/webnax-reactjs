import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";

class Loader {
  handleLoader(loading) {
    console.log("loading ===> ", loading)
    return(
      loading ? <p className="display-1">LOADER</p> : null
    )
  }
}

export default new Loader();
