var bodyLoaded = false;

function SetBodyLoad() {
    bodyLoaded = true
}

function CallLoadFunctions() {
    getCurrentDate();
    SetBodyLoad();
    SetProgramSchoolMajor()
};

function getCurrentDate() {
    if (document.getElementById("hfDate")) {
        var a = new Date();
        document.getElementById("hfDate").value = a.getHours() + ":" + a.getMinutes() + ":" + a.getSeconds()
    }
};

function FillCode(h, e) {
    e.value = h[h.selectedIndex].value;
    var f = h[h.selectedIndex].value;
    var g = f.split("-");
    e.value = g[0];
    if (e.value == "0") {
        e.value = ""
    }
}

function FillCodeNew() {
    var c;
    var b;
    c = document.getElementById("Phone_Country_Code");
    b = document.getElementById("TelePhone_Code");
    b.value = c[c.selectedIndex].value;
    var a = c[c.selectedIndex].value;
    var d = a.split("-");
    b.value = d[0];
    if (b.value == "0") {
        b.value = ""
    }
};

function queryString(b) {
    var c = new a(window.location.search);
    return unescape(c.getValue(b));

    function a(e) {
        if (e.length > 1) {
            this.q = e.substring(1, e.length)
        } else {
            this.q = null
        }
        this.keyValuePairs = new Array();
        if (e) {
            for (var d = 0; d < this.q.split("&").length; d++) {
                this.keyValuePairs[d] = this.q.split("&")[d]
            }
        }
        this.getKeyValuePairs = function() {
            return this.keyValuePairs
        };
        this.getValue = function(g) {
            for (var f = 0; f < this.keyValuePairs.length; f++) {
                if (this.keyValuePairs[f].split("=")[0] == g) {
                    return this.keyValuePairs[f].split("=")[1]
                }
            }
            return false
        };
        this.getParameters = function() {
            var f = new Array(this.getLength());
            for (var g = 0; g < this.keyValuePairs.length; g++) {
                f[g] = this.keyValuePairs[g].split("=")[0]
            }
            return f
        };
        this.getLength = function() {
            return this.keyValuePairs.length
        }
    }
}

function QueryStringSelection() {    
}

function jf_ShowNHideStarting(a, d) {
    var c = document.getElementById("trTime");
    var b = document.getElementById("ddlStarting");
    if (b.value == "2") {
        c.style.display = "Block"
    } else {
        c.style.display = "none"
    }
}
var userIP = "";
getuserIP();


/*LandingTrackReferral STARTS*/
function createCookie(j, i, h) {
    if (h) {
        var f = new Date();
        f.setTime(f.getTime() + (h * 24 * 60 * 60 * 1000));
        var g = "; expires=" + f.toGMTString()
    } else {
        var g = ""
    }
    document.cookie = j + "=" + i + g + "; path=/"
}

function readCookie(c) {
    var i = c + "=";
    var g = document.cookie.split(";");
    for (var j = 0; j < g.length; j++) {
        var h = g[j];
        while (h.charAt(0) == " ") {
            h = h.substring(1, h.length)
        }
        if (h.indexOf(i) == 0) {
            return h.substring(i.length, h.length)
        }
    }
    return null
}

function prepareFrame() {
    if (readCookie("nm")) {} else {
        var j = getParameterByName("f");
        if (j != "") {
            createCookie("nm", j, 365)
        }
    }
    if (readCookie("em")) {} else {
        var e = getParameterByName("e");
        if (e != "") {
            createCookie("em", e, 365)
        }
    }
    if (readCookie("pn")) {} else {
        var i = getParameterByName("p");
        if (i != "") {
            createCookie("pn", i, 365)
        }
    }
    if (readCookie("mustppc")) {} else {
        var l = getParameterByName("ppc");
        var h = "-1";
        if (l != "") {
            h = l
        }
        createCookie("mustppc", h, 365)
    }
    if (readCookie("mustreferer")) {} else {
        var k = parent.document.referrer;
        if (k == "") {
            k = document.referrer
        }
        if (k != "") {
            createCookie("mustreferer", k, 365)
        }
    }
};
/*LandingTrackReferral ENDS*/


/*CUSTOM FUNCTIONS STARTS*/
function getParameterByName(d) {
    d = d.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var e = new RegExp("[\\?&]" + d + "=([^&#]*)"),
        f = e.exec(location.search);
    return f == null ? "" : decodeURIComponent(f[1].replace(/\+/g, " "))
}
window.onload = prepareFrame;

function autofocus2(field, limit, next, evt) {
    evt = (evt) ? evt : event;
    var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode : ((evt.which) ? evt.which : 0));
    if (charCode > 31 && field.value.length == limit && next != 'null') {
        field.form.elements[next].focus();
    }
}

function autofocus(field, limit, next, evt, previous) {
    evt = (evt) ? evt : event;
    var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode : ((evt.which) ? evt.which : 0));
    if (charCode == 8 && field.value.length == 0 && previous != 'null') {
        field.form.elements[previous].focus();
        field.form.elements[previous].value = field.form.elements[previous].value;
    }
    if (charCode > 31 && field.value.length == limit && next != 'null') {
        field.form.elements[next].focus();
    }
}
/*CUSTOM FUNCTIONS ENDS*/

function ValidateConfirmPassword() {
    var b = document.getElementById("txtPassword").value;
    var c = document.getElementById("spanError");
    document.getElementById("spanError").innerHTML = "";
    var a = document.getElementById("txtConfirmPassword").value;
    if (b != a) {
        document.getElementById("spanError").innerText = "Confirm password does not match with the password you created. Please enter password again";
        document.getElementById("spanError").className = "Verd10";
        document.getElementById("tbl_Error").style.display = "block";
        window.scrollTo(0, document.getElementById("tbl_Error").offsetTop)
    } else {
        document.getElementById("spanError").innerText = "";
        document.getElementById("tbl_Error").style.display = "none"
    }
}

function jf_ShowNHideDescription(a, d) {
    var c = document.getElementById("trDescription");
    var b = document.getElementById("Student_Description_Code");
    if (b.value == "8") {
        c.style.display = "Block"
    } else {
        c.style.display = "none"
    }
}

function jf_ShowNHideInterest(a, d) {
    var c = document.getElementById("trInterest");
    var b = document.getElementById("Program_Interest_Code");
    if (b.value == "5") {
        c.style.display = "Block"
    } else {
        c.style.display = "none"
    }
}

function jf_ShowNHideBlock(a, d) {
    var c = document.getElementById("trParents");
    var b = document.getElementById("Age_Below_18");
    if (b.value == "1") {
        c.style.display = "Block"
    } else {
        c.style.display = "none"
    }
}

function StringValidation(b) {
    var a = /^[a-zA-Z+\s]+$/;
    if (a.test(b)) {
        return true
    }
    return false
}

function IntegerValidation9(c) {
    var b = /^\d{9}$/;
    var a = /^\d{10}$/;
    if (b.test(c) || a.test(c)) {
        return true
    } else {
        return false
    }
}

function IntegerValidation(b) {
    var a = /^\d*$/;
    if (a.test(b)) {
        return true
    } else {
        return false
    }
}

function ScriptValidation(b) {
    var a = /[<]+[0-9a-zA-Z \S]+[>]/;
    if (a.test(b)) {
        return true
    } else {
        return false
    }
};

function stripCharsInBag(d, e) {
    var b;
    var a = "";
    for (b = 0; b < d.length; b++) {
        var f = d.charAt(b);
        if (e.indexOf(f) == -1) {
            a += f
        }
    }
    return a
}

function daysInFebruary(a) {
    return (((a % 4 == 0) && ((!(a % 100 == 0)) || (a % 400 == 0))) ? 29 : 28)
}

function DaysArray(b) {
    for (var a = 1; a <= b; a++) {
        this[a] = 31;
        if (a == 4 || a == 6 || a == 9 || a == 11) {
            this[a] = 30
        }
        if (a == 2) {
            this[a] = 29
        }
    }
    return this
}

function validateDate(d) {
    var a = DaysArray(12);
    var e = d.indexOf(dtCh);
    var c = d.indexOf(dtCh, e + 1);
    var h = d.substring(0, e);
    var g = d.substring(e + 1, c);
    var f = d.substring(c + 1);
    strYr = f;
    if (g.charAt(0) == "0" && g.length > 1) {
        g = g.substring(1)
    }
    if (h.charAt(0) == "0" && h.length > 1) {
        h = h.substring(1)
    }
    for (var b = 1; b <= 3; b++) {
        if (strYr.charAt(0) == "0" && strYr.length > 1) {
            strYr = strYr.substring(1)
        }
    }
    month = parseInt(h);
    day = parseInt(g);
    year = parseInt(strYr);
    if (e == -1 || c == -1) {
        return false
    }
    if (h.length < 1 || month < 1 || month > 12) {
        Count++;
        message += "Please Select Valid Date of Birth\n"
    }
    if (g.length < 1 || day < 1 || day > 31 || (month == 2 && day > daysInFebruary(year)) || day > a[month]) {
        return false
    }
    if (f.length != 4 || year == 0 || year < minYear || year > maxYear) {
        return false
    }
    if (d.indexOf(dtCh, c + 1) != -1 || isInteger(stripCharsInBag(d, dtCh)) == false) {
        return false
    }
    return true
};

function EmailAddressValidation(obj) {
    /*@cc_on@*/
    var emailcheck = /^([a-zA-Z0-9])+([\.a-zA-Z0-9_-])*@([a-zA-Z0-9-])+(\.[a-zA-Z0-9_-]+)+$/;
    if (emailcheck.test(obj)) {
        return true
    } else {
        return false
    }
}

function GoToNext(c, e, b, d) {
    pathArray = window.location.pathname.split("?");
    newURL = window.location.protocol + "//" + window.location.host;
    var a = newURL + pathArray[0] + "?s=" + c + "&m=" + e + "&p=0";
    document.getElementById("keyword4").value = b + " " + d;
    document.location.href = a
}

function jf_ShowNHideStarting(a, d) {
    var c = document.getElementById("trTime");
    var b = document.getElementById("ddlStarting");
    if (b.value == "2") {
        c.style.display = "Block"
    } else {
        c.style.display = "none"
    }
}


/*SIGNUP (ONE STEP FORM)*/
function ValidateApplyNow(Agency_Code, AgencySite_Code, signupthroughcode, signuptype, thisparent, $this) {
debugger;
    
    var $parent = $($this).parents("." + thisparent);
    
    
    
    $("#First_Name").val($($parent).find(".cname").val());
    $("#Email_Address").val($($parent).find(".cemail").val());
    $("#Code").val($($parent).find(".cphonecode").val());
    $("#phone").val($($parent).find(".cphone").val());
    $("#Message").val($($parent).find(".cmessage").val());
    $("#CompanyName").val($($parent).find(".cCompanyName").val());

    if($("#Code").val() == '') {
        $("#Code").val($($parent).find("input[name=pc]").val().replace(" +",""));
    }
    
    if($("#hdnPhoneCode").val() == '') {
        $("#hdnPhoneCode").val($($parent).find("input[name=pc]").val().replace(" +",""));
    }
    
    if($("#hdnCountryName").val() == '') {
        $("#hdnCountryName").val($($parent).find("input[name=ctry]").val());
    }
    
    var i = 0;
    var l = 0;
    var m = "";

    if (document.getElementById("First_Name")) {
        if (document.getElementById("First_Name").value == "" || document.getElementById("First_Name").value == "Your Full Name") {
            i++;
            m += "Please Provide Full Name\n"
        } else {
            if (ScriptValidation(document.getElementById("First_Name").value)) {
                i++;
                m += "Please Provide Valid Full Name\n"
            }
        }
    }
    if (document.getElementById("Email_Address")) {
        if (document.getElementById("Email_Address").value.replace(/\s+/g, "") == "" || document.getElementById("Email_Address").value == "Email Address") {
            i++;
            m += "Please Provide Email Address\n"
        } else {
            if (!EmailAddressValidation(document.getElementById("Email_Address").value)) {
                i++;
                m += "Please Provide Valid Email Address\n"
            }
        }
    }
    if (document.getElementById("Code")) {
        if (document.getElementById("Code").value == "0") {
            i++;
            m += "Please Select Your Country\n"
        }
    }
    

    if (document.getElementById("phone")) {
        if (document.getElementById("phone").value == "" || document.getElementById("phone").value == "Enter Area Code + Phone No." || document.getElementById("phone").value == "Area Code + Phone No.") {
            i++;
            m += "Please Provide Phone Number\n"
        }
		else if (document.getElementById("phone").value.length < 8 ) {
            i++;
            m += "Phone Number length should be greater than 8\n"
        }
    }
	
	
    
    if (document.getElementById("ddlType")) {
        if (document.getElementById("ddlType").value == "2") {
            if (document.getElementById("CompanySize").value == "") {
                i++;
                m += "Please Provide Company Name\n"
            }
            if (document.getElementById("ddlCompanySize").value == "") {
                i++;
                m += "Please Provide Company Size\n"
            }
        }
    }
    
    if (document.getElementById("Message")) {
        if (document.getElementById("Message").value == "") {
            i++;
            m += "Please Provide Message\n"
        }
    }
    
    if (document.getElementById("hdnService")) {
        if (document.getElementById("hdnService").value == "") {
            i++;
            m += "Select any Service\n"
        }
    }

    if (document.getElementById("ddlDate")) {
        if (document.getElementById("ddlDate").value == "") {
            i++;
            m += "Please Select Birth Day\n"
        }
    }
    if (document.getElementById("ddlMonth")) {
        if (document.getElementById("ddlMonth").value == "") {
            i++;
            m += "Please Select Birth Month\n"
        }
    }
    if (document.getElementById("ddlYear")) {
        if (document.getElementById("ddlYear").value == "") {
            i++;
            m += "Please Select Birth Year\n"
        }
    }    

    if (document.getElementById("ddlNationality")) {
        if (document.getElementById("ddlNationality").value == "") {
            i++;
            m += "Please Select Your Nationality\n"
        }
    }

    if (i > 0) {
        alert(m);
        return false
    } else {
        // Addstudent(Agency_Code, AgencySite_Code, signupthroughcode, signuptype, $($parent));
        EmailExisits($parent);		
        createCookie("nm", document.getElementById("First_Name").value, 365);
        createCookie("em", document.getElementById("Email_Address").value, 365);        
        createCookie("pn", document.getElementById("phone").value, 365);
        $($parent).find('.submitbtn').addClass('submitted');
        $($parent).find('.loader-overlay').addClass('active');
        return true
    }

}
 

function EmailExisits($parent)
{
    debugger;
    var hdnSiteCode = $("#hdnSiteCode").val();
    var Email_Address = $("#Email_Address").val();  
    var datavalue = "EmailAddress=" + Email_Address + "&AgencySiteCode=" + hdnSiteCode
    var apiurl = "https://www.ingic.com/INGICSignupAPI/api/Students/GetEmailAddressExist?";
    $.ajax({
        url: apiurl,
        type: 'GET',
        data: datavalue,
        dataType: "jsonp",
        contentType: "application/json;charset=utf-8",
        success: function (data) {
            if (data.indexOf("|") > -1)
            {
				debugger;
                var b = data.split("|")[0];
                var c = data.split("|")[1];
                    if (c == "AE") {
                        alert("Email already exists.");
                        $($parent).find('.submitbtn').removeClass('submitted');
                        $($parent).find('.loader-overlay').removeClass('active');
                    } 
                    else {
                        $($parent).find("#btnSubmitform").click();  
                    }
            }
        },
        error: function (data) {
            onSuccess(data);
        }
    });
}

function ValidateApplyNowPopup(Agency_Code, AgencySite_Code, signupthroughcode, signuptype) {

    var i = 0;
    var l = 0;
    var m = "";

    if (document.getElementById("First_Name")) {
        if (document.getElementById("First_Name").value == "" || document.getElementById("First_Name").value == "Your Full Name") {
            i++;
            m += "Please Provide Full Name\n"
        } else {
            if (ScriptValidation(document.getElementById("First_Name").value)) {
                i++;
                m += "Please Provide Valid Full Name\n"
            }
        }
    }
    if (document.getElementById("Email_Address")) {
        if (document.getElementById("Email_Address").value.replace(/\s+/g, "") == "" || document.getElementById("Email_Address").value == "Email Address") {
            i++;
            m += "Please Provide Email Address\n"
        } else {
            if (!EmailAddressValidation(document.getElementById("Email_Address").value)) {
                i++;
                m += "Please Provide Valid Email Address\n"
            }
        }
    }
    if (document.getElementById("Code")) {
        if (document.getElementById("Code").value == "0") {
            i++;
            m += "Please Select Your Country\n"
        }
    }
    

    if (document.getElementById("phone")) {
        if (document.getElementById("phone").value == "" || document.getElementById("phone").value == "Enter Area Code + Phone No." || document.getElementById("phone").value == "Area Code + Phone No.") {
            i++;
            m += "Please Provide Phone Number\n"
        }
    }
    
    if (document.getElementById("Message")) {
        if (document.getElementById("Message").value == "") {
            i++;
            m += "Please Provide Message\n"
        }
    }

    if (document.getElementById("ddlDate")) {
        if (document.getElementById("ddlDate").value == "") {
            i++;
            m += "Please Select Birth Day\n"
        }
    }
    if (document.getElementById("ddlMonth")) {
        if (document.getElementById("ddlMonth").value == "") {
            i++;
            m += "Please Select Birth Month\n"
        }
    }
    if (document.getElementById("ddlYear")) {
        if (document.getElementById("ddlYear").value == "") {
            i++;
            m += "Please Select Birth Year\n"
        }
    }    

    if (document.getElementById("ddlNationality")) {
        if (document.getElementById("ddlNationality").value == "") {
            i++;
            m += "Please Select Your Nationality\n"
        }
    }   

    if (i > 0) {
        alert(m);
        return false
    } else {
        AddstudentPopup(Agency_Code, AgencySite_Code, signupthroughcode, signuptype);
        createCookie("nm", document.getElementById("First_Name").value, 365);
        createCookie("em", document.getElementById("Email_Address").value, 365);        
        createCookie("pn", document.getElementById("phone").value, 365);
        return true
    }

}


//////////// VALIDATION FUNCTIONS ENDS //////////////////

/*API CALLING STARTS*/

function getuserIP() {
    try {
        $.get("https://jsonip.com", function(a) {
            userIP = a.ip
        })
    } catch (b) {
        userIP = "103.255.110.13"
    }
}

function getPassword() {
    var a = "";
    var b = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var c = 0; c < 8; c++) {
        a += b.charAt(Math.floor(Math.random() * b.length))
    }
    return a
};

function AddstudentPopup(Agency_Code, AgencySite_Code, signupthroughcode, signuptype) {
    
    var Selected_Program_code = null;
    var Selected_School_code = null;
    var Selected_Major_code = null;
    var Selected_Program_Name = null;
    var Selected_School_Name = null;
    var Selected_Major_Name = null;
    var Education_Level = null;
    var CountryofEducation_Code = "0";
    var CountryofEducation = null;
    var NameofDegree = null;
    var Major = null;
    var HighestEduLevel = "0";
    var Institute = null;
    var Applyingfor = null;
    var pwd = null;
    var DOB = null;
    var First_Name = null;
    var Email_Address = null;
    var Gender_Code = null;
    var Telephone_Number = null;
    var Telephone_Code = null;
    var Country_Code = null;
    var Country_Name = null;
    var Phone_Type_Code = null;
    var Nationality_Code = null;
    var Nationality = null;
    var Salary_code = null;
    var Salary = null;
    var Work_Experience_Code = null;
    var Work_Experience = null;
    var Client_Date = null;
    var location = null;
    var mustppc = "-1";
    var referalpk = "-1";
    var referrer = "";
    var AgencyCode = null;
    var AgencyName = null;
    var Student_Code = "0";
    var CustomerMessage = "";
    var AgencySiteCode = null;
    if (signuptype == 1) {
        First_Name = $('#First_Name').val();
        Email_Address = $('#Email_Address').val();
        Telephone_Number = $('#phone').val();
        Telephone_Code = $(".countrylist option:selected").val();
        Country_Name = $(".countrylist option:selected").attr("data-name");
        Country_Code = $(".countrylist option:selected").val();
        Service = $('#hdnService').val();
        pwd = getPassword();
    }
    if ($('#ddlYear').val() != null && $('#ddlYear').val() != '')
        DOB = new Date($('#ddlYear').val(), $('#ddlMonth').val(), $('#ddlDate').val());
    if ($('#rblGender').val() != null)
        Gender_Code = $('#rblGender input:checked').val()
    if ($('#ddlNationality').val() != null) {
        Nationality_Code = $('#ddlNationality').val().split("-")[0];
        Nationality = $('#ddlNationality option:selected').text();
    }
    if ($('#ddlApplyingfor').val() != null) {
        Applyingfor = $('#ddlApplyingfor').val();
    }
    if ($('#hfStudent_Code').val() != null && $('#hfStudent_Code').val() != '') {
        Student_Code = $('#hfStudent_Code').val();
    }
    if ($('#Message').val() != null && $('#Message').val() != '') {
        CustomerMessage = $('#Message').val();
    }
    
    var apiurl = "https://www.ingic.com/INGICAPI/api/students";
    var student = {
        First_Name: First_Name,
        Email_Address: Email_Address,
        Gender_Code: Gender_Code,
        Date_Of_Birth: DOB,
        Telephone_Number: Telephone_Number,
        Telephone_Code: Telephone_Code,
        Country_Code: Country_Code,
        Country_Name: Country_Name,
        Password: pwd,
        Signup_type_Code: signuptype,
        Sign_Up_Through_Code: signupthroughcode,
        Phone_Type_Code: Phone_Type_Code,
        Nationality_Code: Nationality_Code,
        Nationality: Nationality,
        Salary_code: Salary_code,
        Salary: Salary,
        Work_Experience_Code: Work_Experience_Code,
        Work_Experience: Work_Experience,
        Selected_Program_code: Selected_Program_code,
        School_Code: Selected_School_code,
        Major_Code: Selected_Major_code,
        Selected_Program_Name: Selected_Program_Name,
        Selected_School_Name: Selected_School_Name,
        Selected_Major_Name: Selected_Major_Name,
        Education_Level: Education_Level,
        Client_Date: Client_Date,
        Client_Code: 0,
        Cookie_mustppc: mustppc,
        Cookie_mustreferer: referrer,
        Cookie_ReferalPK: referalpk,
        Page_Url: location,
        Student_IP: userIP,
        Portal_Code: 0,
        Student_Code: Student_Code,
        Student_Id: "",
        Agent_Code: "-1",
        CountryofEducation_Code: CountryofEducation_Code,
        CountryofEducation: CountryofEducation,
        NameofDegree: NameofDegree,
        Major: Major,
        HighestEduLevel: HighestEduLevel,
        Institute: Institute,
        Applyingfor: Applyingfor,
        AgencyCode: Agency_Code,
        AgencyName: AgencyName,
        CustomerMessage: CustomerMessage,
        AgencySiteCode: AgencySite_Code
    };

            var MustRefrer = null;
    if (readCookie("mustreferer")){
        MustRefrer = readCookie("mustreferer").replace('&','|');
    }
    
    var ThisPageURL = window.location.href.replace('&','|');

    var datavalue = "MustRefrer=" + MustRefrer + "&PageURL=" + ThisPageURL + "&stringitem=" + JSON.stringify(student);
    
    console.log(student)
    console.log(datavalue)

    $.ajax({
        url: apiurl,
        type: 'GET',
        data: datavalue,
        dataType: "jsonp",
        contentType: "application/json;charset=utf-8",
        success: function (data) {
            if (signuptype == 1)
                onSuccess(data);
        },
        error: function (data) {
            onSuccess(data);
        }
    });
}

function Addstudent(Agency_Code, AgencySite_Code, signupthroughcode, signuptype, $parent) {
    var Selected_Program_code = null;
    var Selected_School_code = null;
    var Selected_Major_code = null;
    var Selected_Program_Name = null;
    var Selected_School_Name = null;
    var Selected_Major_Name = null;
    var Education_Level = null;
    var CountryofEducation_Code = "0";
    var CountryofEducation = null;
    var NameofDegree = null;
    var Major = null;
    var HighestEduLevel = "0";
    var Institute = null;
    var Applyingfor = null;
    var pwd = null;
    var DOB = null;
    var First_Name = null;
    var Email_Address = null;
    var Gender_Code = null;
    var Telephone_Number = null;
    var Telephone_Code = null;
    var Country_Code = null;
    var Country_Name = null;
    var Phone_Type_Code = null;
    var Nationality_Code = null;
    var Nationality = null;
    var Salary_code = null;
    var Salary = null;
    var Work_Experience_Code = null;
    var Work_Experience = null;
    var Client_Date = null;
    var location = null;
    var mustppc = "-1";
    var referalpk = "-1";
    var referrer = "";
    var AgencyCode = null;
    var AgencyName = null;
    var Student_Code = "0";
    var CustomerMessage = "";
    var AgencySiteCode = null;

    if (signuptype == 1) {
        First_Name = $('#First_Name').val();
        Email_Address = $('#Email_Address').val();
        Telephone_Number = $('#phone').val();
        Telephone_Code = $($parent).find(".countrylist option:selected").val();
        Country_Name = $($parent).find(".countrylist option:selected").attr("data-name");
        Country_Code = $($parent).find(".countrylist option:selected").val();
        pwd = getPassword();
    }

    if ($('#ddlYear').val() != null && $('#ddlYear').val() != '')
        DOB = new Date($('#ddlYear').val(), $('#ddlMonth').val(), $('#ddlDate').val());
    if ($('#rblGender').val() != null)
        Gender_Code = $('#rblGender input:checked').val()
    if ($('#ddlNationality').val() != null) {
        Nationality_Code = $('#ddlNationality').val().split("-")[0];
        Nationality = $('#ddlNationality option:selected').text();
    }
    if ($('#ddlApplyingfor').val() != null) {
        Applyingfor = $('#ddlApplyingfor').val();
    }
    if ($('#hfStudent_Code').val() != null && $('#hfStudent_Code').val() != '') {
        Student_Code = $('#hfStudent_Code').val();
    }
    if ($('#Message').val() != null && $('#Message').val() != '') {
        CustomerMessage = $('#Message').val();
    }
    
    var apiurl = "https://www.ingic.com/INGICAPI/api/students";
    var student = {
        First_Name: First_Name,
        Email_Address: Email_Address,
        Gender_Code: Gender_Code,
        Date_Of_Birth: DOB,
        Telephone_Number: Telephone_Number,
        Telephone_Code: Telephone_Code,
        Country_Code: Country_Code,
        Country_Name: Country_Name,
        Password: pwd,
        Signup_type_Code: signuptype,
        Sign_Up_Through_Code: signupthroughcode,
        Phone_Type_Code: Phone_Type_Code,
        Nationality_Code: Nationality_Code,
        Nationality: Nationality,
        Salary_code: Salary_code,
        Salary: Salary,
        Work_Experience_Code: Work_Experience_Code,
        Work_Experience: Work_Experience,
        Selected_Program_code: Selected_Program_code,
        School_Code: Selected_School_code,
        Major_Code: Selected_Major_code,
        Selected_Program_Name: Selected_Program_Name,
        Selected_School_Name: Selected_School_Name,
        Selected_Major_Name: Selected_Major_Name,
        Education_Level: Education_Level,
        Client_Date: Client_Date,
        Client_Code: 0,
        Cookie_mustppc: mustppc,
        Cookie_mustreferer: referrer,
        Cookie_ReferalPK: referalpk,
        Page_Url: location,
        Student_IP: userIP,
        Portal_Code: 0,
        Student_Code: Student_Code,
        Student_Id: "",
        Agent_Code: "-1",
        CountryofEducation_Code: CountryofEducation_Code,
        CountryofEducation: CountryofEducation,
        NameofDegree: NameofDegree,
        Major: Major,
        HighestEduLevel: HighestEduLevel,
        Institute: Institute,
        Applyingfor: Applyingfor,
        AgencyCode: Agency_Code,
        AgencyName: AgencyName,
        CustomerMessage: CustomerMessage,
        AgencySiteCode: AgencySite_Code
    };

            var MustRefrer = null;
    if (readCookie("mustreferer")){
        MustRefrer = readCookie("mustreferer").replace('&','|');
    }
    
    var ThisPageURL = window.location.href.replace('&','|');

    var datavalue = "MustRefrer=" + MustRefrer + "&PageURL=" + ThisPageURL + "&stringitem=" + JSON.stringify(student);
    
    console.log(student)
    console.log(datavalue)

    $.ajax({
        url: apiurl,
        type: 'GET',
        data: datavalue,
        dataType: "jsonp",
        contentType: "application/json;charset=utf-8",
        success: function (data) {
            if (signuptype == 1)
                onSuccess(data);
        },
        error: function (data) {
            onSuccess(data);
        }
    });
}

function onSuccess_moveToNext(a) {
    
    if (a.indexOf("|") > -1) {
        var b = a.split("|")[0];
        var c = a.split("|")[1];
        if (c == "Y") {
            document.getElementById("hfStudent_Code").value = b;
            moveToNextStep();
        } else {
            if (c == "AE") {
                $("input[name=Email_Address]").addClass("error");
                alert("Email already exists.");
            } else {
                alert("Sorry! Some error has occurred!!")
            }
        }
    } else {
        alert("Sorry! Some error has occurred.")
    }
}

function onSuccess(a) {
    
    if (a.indexOf("|") > -1) {
        var b = a.split("|")[0];
        var c = a.split("|")[1];
        if (c == "Y") {
            //document.getElementById("hfStudent_Code").value = b;
            moveToThanks();
        } else {
            if (c == "AE") {
                //$("input[name=Email_Address]").addClass("error");
                alert("Email already exists.");
            } else {
                alert("Sorry! Some error has occurred!")
            }
        }
    } else {
        alert("Sorry! Some error has occurred.")
    }
}

function onSuccess_Getsignup(a) {
    
    if (a.indexOf("|") > -1) {
        var b = a.split("|")[0];
        var c = a.split("|")[1];
        if (b == "Y") {
            var First_Name = a.split("|")[2];
            document.getElementById("First_Name").value = First_Name;
            var Email_Address = a.split("|")[3];
            document.getElementById("Email_Address").value = Email_Address;
            var TelePhone_Code = a.split("|")[4];
            document.getElementById("TelePhone_Code").value = TelePhone_Code;
            var txtPhone = a.split("|")[5];
            document.getElementById("txtPhone").value = txtPhone;
            var Phone_Country_Code = a.split("|")[6];
            document.getElementById("Phone_Country_Code").value = TelePhone_Code + "-" + Phone_Country_Code;
        }
    }
}

function moveToThanks()
{
    window.parent.document.location.href = '/thankyou/';
}

function onError(a) {
    alert('Error');
}

function onError4(a) {}
/*API CALLING ENDS*/

/***********UI Related Functions*************/
/*Loading Icons external CSS files*/
loadcss = document.createElement('link');
loadcss.setAttribute("rel", "stylesheet");
loadcss.setAttribute("type", "text/css");
loadcss.setAttribute("href", "https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css");
document.getElementsByTagName("head")[0].appendChild(loadcss);

/*Field Value show hide on input focus*/
$(document).ready(function() {
    $("input:text,textarea,input:password").each(function() {
        var a = this.value;
        $(this).focus(function() {
            if (this.value == a) {
                this.value = ""
            }
        });
        $(this).blur(function() {
            if (this.value == "") {
                this.value = a
            }
        })
    });
    $("iframe").attr("allowtransparency", "true")
    
    $("#hdnPassword").val(getPassword());
    
    var MustRefrer = null;
    if (readCookie("mustreferer")){
        $("#hdnMustReferer").val(readCookie("mustreferer"));
    }
    
    $("#hdnPageURL").val(window.location.href);

    $('select.countrylist').change(function () {

        var ctabbr = $('select.countrylist option:selected').attr('data-abbr');
        var ctcode = $('select.countrylist option:selected').attr('value');
        var ctname = $('select.countrylist option:selected').attr('data-name');

        $('input.cphonecode').val(ctabbr + ' +' + ctcode);

        $('input#hdnPhoneCode').val(ctcode);
        $('input#hdnCountryAbbr').val(ctabbr);
        $('input#hdnCountryName').val(ctname);

    });
    
    $(".cService").change(function(){
        $('#hdnService').val($(this).val());
    });
    
    $(".ddlCompanySize").change(function(){
        $('#CompanySize').val($(this).val());
    });

    $('select.ddlType').change(function () {
        if ($(this).val() == '1')
            $('.divCompanyDetails').hide();
        else
            $('.divCompanyDetails').show();
    });

    $.ajax({
        url: 'https://telize-v1.p.mashape.com/geoip',
        type: 'GET',
        data: {},
        dataType: 'json',
        success: function (data) {
            $(".countrylist option").each(function () {
                if ($(this).attr("data-abbr") == data.country_code) {
                    var val = $(this).val();
                    $(this).attr("selected", "selected");
                    var ctabbr = $('select.countrylist option:selected').attr('data-abbr');
                    var ctcode = $('select.countrylist option:selected').attr('value');
                    var ctname = $('select.countrylist option:selected').attr('data-name');
                    $('input.cphonecode').val(ctabbr + ' +' + ctcode);

                    $('input#hdnPhoneCode').val(ctcode);
                    $('input#hdnCountryAbbr').val(ctabbr);
                    $('input#hdnCountryName').val(ctname);
                }
            });
            $('input[name=hdndefaultCountry]').val(JSON.stringify(data));
        },
        error: function (err) {
            //alert("")
        },
        beforeSend: function (xhr) {
            xhr.setRequestHeader("X-Mashape-Authorization", "qKsg8tYMdTmshjZ0eSZznAWBhwOFp1huvy7jsnNg3rhw4x8SGD"); // Enter here your Mashape key
        }
    });
});
