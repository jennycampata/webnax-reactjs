import $ from 'jquery';

var pageWidth = $(window).width();
$(function(){

 $('select[multiple]').multiselect({
    texts    : {
        placeholder: 'Select Services'
    }
});
    

$(".load-more").click(function(){
    $(".show-more").fadeIn();
    $(this).parent().hide();
});
    
    $(document).scroll(function(){
        var y = $(this).scrollTop();
        if (y > 500){
            $('').fadeIn();
        } else {
            $('').fadeOut();
        }

        var vscroll = $(window).scrollTop();

        if (vscroll > 80) {
            $(".header").addClass("scroll");
        } else {
            $(".header").removeClass("scroll");
        }

        if (vscroll >= 600) {
            $(".request-sidebtn").show();
        } else {
            $(".request-sidebtn").hide();
        }
        
    });


    // 
    // $('.popUpBtn').click(function() {
    //     alert();
    // });
    $('[data-fancybox="video-file"]').fancybox({
      iframe : {
        css : {
          width : '75%',
          height : '100%'
        }
      }
    }); 
    // $('#open-website').lightGallery({
    //     selector: 'this'
    //     iframe : {
    //         css : {
    //           width : '75%',
    //           height : '100%'
    //         }
    //       }
    // });

   $('#viewmoreldp').on('click', function (e) {
            e.preventDefault(); 
            var visibleDivs=$(".client-logo-main li:visible").length;
            var totalVis=$(".client-logo-main li").length;
            var a=totalVis-visibleDivs;

            $('.client-logo-main li:hidden').slice(0, 9).slideDown("slow");
           

            if (a <= 1) {
              
                $("#viewmoreldp").fadeOut('slow');
            }

});


    $('.portfolio-show-all').slideUp();
    $('.loadmore-portfolio').click(function() {
        $('.works-wrapper .desc-first').css("display", "inline-block");
        $(this).fadeOut();
    });

    var pageclass  = $('body').attr('class');
    if(pageclass == "home-page"){
        $(window).scroll(function() {    
            var scroll = $(window).scrollTop();

            
            if (scroll >= 41) {
                $(".phnew").addClass("ph");
                $(".ph").removeClass("phnew");
                $(".pnew").addClass("pn");
                $(".pn").removeClass("pnew");
            } else {
                $(".ph").addClass("phnew");
                $(".phnew").removeClass("ph");
                $(".pn").addClass("pnew");
                $(".pnew").removeClass("pn");

            }

            if($('body').width() < 991) {
                $('.ph').removeClass("fixed");
            }
        });
    }
    $('.requestinfo-scroll').click(function(){
        $("html, body").animate({ scrollTop: 440 }, 2000);
        return false;
    });


    //*****************************
    // Portfolio Gallery Slider
    //*****************************
    var loopDuration=12000;
            
    $(".portfolio-scroll-slider").mThumbnailScroller({
      speed: 6
    });
      

    //*****************************
    // Match Height Functions
    //*****************************
    $('.matchheight').matchHeight();

    function checkWidth() {
        var windowSize = $(window).width();

        if (windowSize > 767) {
            $('.openpopupform').click(function(){
                $('.popup-form-up').fadeIn();
                $('.form-overlay').fadeIn();
            });
            
            $('.form-overlay').click(function(){
                $(this).fadeOut();
                $('.popup-form-up').fadeOut();
            });
        }
        else {
            $('.openpopupform').click(function(){
                $('html, body').animate({
                    scrollTop: $(".cntctBtm").offset().top - 40
                }, 2000);
            });
        }
    }

    // Execute on load
    checkWidth();

    
    var toggleSlide = function(){
        $(".leftArea li.liveActive").removeClass().next().add(".leftArea li:first").last().addClass("liveActive");
    }
    setInterval(toggleSlide, 3000);

    //*****************************
    // Mobile Navigation
    //*****************************
    $('.mobile-nav-btn').click(function() {
        $('.mobile-nav-btn, .mobile-nav, .app-container, .mobile-cta, .frameBar, header.mainhead, .headlogo').toggleClass('active');
    });

    $('.firstlevel li a').click(function() {
        if($(this).hasClass('active')){
            $(this).removeClass('active');
            $(this).siblings('ul').slideUp();
        }else{
            $('.firstlevel li a').removeClass('active');
            $(this).addClass('active');
            $('.dropdown', $(this)).slideUp();
            $(this).siblings('ul').slideDown();
        }
    });

    $('.mainnav > li > a').click(function() {
        if($(this).hasClass('active')){
            $(this).removeClass('active');
            $(this).parents('li').children('.firstlevel').slideUp();
        }else{
            $(this).addClass('active');
            $(this).parents('li').children('.firstlevel').slideDown();
            $(this).parents('li').siblings('li').children('a').removeClass('active');
            $(this).parents('li').siblings('li').children('.firstlevel').slideUp();
        }
    });

    xfilebrowse('.file-upload');
    function xfilebrowse(tgt){  
        $(tgt+' input[type=file]').each(function() {
            $(this).wrap('<div class="upldwrap" />');
            $(this).parent().append('<span class="browse">Choose Files</span> <label class="filelabel">Upload your files</label>');
            $(this).css('opacity', 0);
            $(this).on('change', function() {
                var txt = $(this).val();
                if(txt !== ''){
                    txt = txt.replace(/\\/g, '/').replace(/.*\//, '');
                    $(this).siblings('.filelabel').html(txt);
                }else{
                    $(this).siblings('.filelabel').html('No File Selected');
                }                
            })
        });
    }


    var getregion = window.location.pathname;
    getregion = getregion.split('/');
    var regionvalue = getregion[2];
    $('.region-txt').text(regionvalue);  


    //*****************************
    // Portfolio Page Tabs
    //*****************************

        $('.lightgallery2').lightGallery({
        thumbnail:false,
        controls:false,
        counter:false,
        autoplayControls:false,  
        keyPress:false,
        enableDrag:false, 
        mousewheel:false,       
    });
        $('.lightgallery').lightGallery({
        thumbnail:false,
        controls:false,
        counter:false,
        autoplayControls:false,  
        keyPress:false,
        enableDrag:false, 
        mousewheel:false,
    });

    $('.video-gallery').lightGallery();

    
    //*****************************
    // Default Function
    //*****************************
    $('[href="#"]').attr("href", "javascript:;");


    //*****************************
    // Top Tabbing
    //*****************************
    $('.tabbing-link ul li').first().addClass('current');
    $('.tab-content').first().addClass('current');

    $('.tabbing-link ul li').click(function(){
        //console.log('tab:'+x);
        //$('data-countit');
        var tab_id = $(this).attr('data-tab');

        $('.tabbing-link ul li').removeClass('current');
        $('.tab-content').removeClass('current');

        $(this).addClass('current');
        $("#"+tab_id).addClass('current');
    });


    //*****************************
    // for data-img
    //*****************************
    // var list = document.querySelectorAll("div[data-image]");

    //     for (var i = 0; i < list.length; i++) {
    //       var url = list[i].getAttribute('data-image');
    //       list[i].style.backgroundImage="url('" + url + "')";
    //     }


    //*****************************
    // Tabbing New
    //*****************************
    $('.tabbing-link li').first().addClass('current');
    $('.tabbing-content .tab-content').first().addClass('current');

    $('.tabbing-link li').click(function(){
        $('.tabbing-link li').removeClass('current');
        $('.tabbing-content .tab-content').removeClass('current');
        $(this).addClass('current');
        var tab_id = $(this).index();
        tab_id+=1;
        $('.tabbing-content .tab-content:nth-child('+tab_id+')').addClass('current');
    });


    //*****************************
    // Scroll Funtion
    //*****************************

    $(window).scroll(function() {    
        var scroll = $(window).scrollTop();

        
        if (scroll >= 41) {
            $(".ph").addClass("fixed");
        } else {
            $(".ph").removeClass("fixed");
        }

        if($('body').width() < 991) {
            $('.ph').removeClass("fixed");
        }

        
        var mydivtop,getval,
        ht = $(this).height() / 3;
        ht = ht * 3 ;
        
        $('[data-srcset]').each(function(e){
            mydivtop = $(this).offset().top - ht;
            
            if(scroll >= mydivtop){
                getval = $(this).attr('data-srcset');
                if(getval !== ''){
                    $(this).attr('src',getval);
                }
                $(this).attr('data-srcset','');
            }
        });
        
    });


    //*****************************
    // Slick Slider
    //*****************************
    // $(".recentdesign-slider").slick({
    //     arrows: false,
    //     dots: false,
    //     infinite:true,
    //     centerMode:true,
    //     responsive: [{
    //         breakpoint: 1999,
    //         settings: "unslick"
    //     }, {
    //         breakpoint: 991,
    //         settings: {
    //             slidesToShow: 3;
    //             slidesToScroll: 1;
    //             arrows: false
    //         }
    //     }]

    // });
     $('.wooslider').slick({
      dots: true,
      infinite: true,
      arrows:true,
      speed: 1000,
      vertical: true,
      verticalSwiping: true,
      autoplay: true,
      autoplaySpeed: 4000,
      slidesToShow: 1,
      adaptiveHeight: false
    });

     $(".slider-for-client").slick({
         slidesToShow: 6,
         infinite: true,
         arrows: true,
         responsive: [{
            breakpoint: 768,
            settings: {
                arrows: true,
                slidesToShow: 1

            }
        }]
         
     });

     $(".main-client-strip-slider").slick({
         slidesToShow: 1,
         arrows: true,
         infinite: false,
         dots: false,
         infinite: true,
          
     });
     $(".mobile-client-strip-slider").slick({
         arrows: true,
         infinite: false,
         dots: false,
         infinite: true,
         slidesToShow: 2,
         slidesToScroll: 2,
         responsive: [{
             breakpoint: 768,
             settings: {
                 arrows: true,
                 slidesToShow: 1,
                 slidesToScroll: 1

             }
         }]              
         
     });

     $('.main-feat-slider').slick({
        arrows: true,
        autoplay: true,
        autoplaySpeed: 3000,
        dots: false
    });

    $(".portfolio-mobile-slider").slick({
        slidesToShow: 4,
        arrows: false,
        infinite: true,
        speed: 300,
        variableWidth: true,
        centerMode: true,
        arrows: true,
        swipe: false,
        responsive: [{
            breakpoint: 768,
            settings: {
                swipe: true,
                autoplay: true,
                autoplaySpeed: 2000
            }
        }]
    });
    $(".seo-text-slider").slick({
        slidesToShow: 1,
        arrows: false,
        infinite: true,
        slidesToScroll: 1,
        speed: 300,
        dots: true
        
    });
    
    $('.work-portfolio-slider').slick({
        dots: false,
        arrows: true,
    });

    $('.portfolio-mobile-slider .slick-center').nextAll('.slick-slide').click(function() {
        $('.slick-next').trigger('click');
    });

    $('.center').slick({
        centerMode: true,
        infinite: true,
        centerPadding: '60px',
        slidesToShow: 4,
        speed: 500,
        responsive: [{
            breakpoint: 768,
            settings: {
                arrows: false,
                centerMode: true,
                centerPadding: '40px',
                slidesToShow: 3
            }
        }, {
            breakpoint: 480,
            settings: {
                arrows: false,
                centerMode: true,
                centerPadding: '40px',
                slidesToShow: 1
            }
        }]
    });
    $('.expertise-sliderr').slick({
        centerMode: false,
        infinite: true,
        dots:true,
        slidesToShow: 4,
        speed: 500,
        responsive: [{
            breakpoint: 768,
            settings: {
                arrows: false,
                dots:true,
                centerMode: false,
                slidesToShow: 1
            }
        }, {
            breakpoint: 480,
            settings: {
                arrows: false,
                dots:true,
                centerMode: false,
                slidesToShow: 1
            }
        }]
    });
    $('.expertise-slider3').slick({
        centerMode: false,
        infinite: true,
        dots:true,
        slidesToShow: 3,
        speed: 500,
        responsive: [{
            breakpoint: 768,
            settings: {
                arrows: false,
                dots:true,
                centerMode: false,
                slidesToShow: 1
            }
        }, {
            breakpoint: 480,
            settings: {
                arrows: false,
                dots:true,
                centerMode: false,
                slidesToShow: 1
            }
        }]
    });
    $('.res-slide2').slick({
      arrows: false,
      dots: true,
      infinite: true,
      autoplay:false,
      speed: 300,
     
      responsive: [
                {
                  breakpoint: 2000,
                  settings: "unslick"
                },
                {
                  breakpoint: 768,
                  settings: {
                    unslick: true,
                    slidesToShow: 2,
                    slidesToScroll: 1
                  }
                },
                {
                  breakpoint: 535,
                  settings: {
                    unslick: true,
                    slidesToShow: 1,
                    slidesToScroll: 1
                  }
                }
            ]
    });

    $('.main-client-slider').slick({
      arrows: true,
      dots: false,
      infinite: true,
      autoplay:true,
      speed: 300,
      responsive: [
                {
                  breakpoint: 2000,
                  settings: "unslick"
                },
                {
                  breakpoint: 768,
                  settings: {
                    unslick: true,
                    slidesToShow: 1,
                    slidesToScroll: 1
                  }
                }
            ]
    });

    $('.iphone-slider2').slick({
      arrows: false,
      dots: true,
      infinite: true,
      autoplay:true,
      speed: 300,
     
      responsive: [
                {
                  breakpoint: 2000,
                  settings: "unslick"
                },
                {
                  breakpoint: 991,
                  settings: {
                    unslick: true,
                    slidesToShow: 2,
                    slidesToScroll: 1
                  }
                },
                {
                  breakpoint: 768,
                  settings: {
                    unslick: true,
                    slidesToShow: 1,
                    slidesToScroll: 1
                  }
                }
            ]
    });




     $(".slider-for-industry").slick({
        slidesToShow: 5,
        infinite: true,
        arrows: true,
        responsive: [{
            breakpoint: 768,
            settings: {
                arrows: true,
                slidesToShow: 1

            }
        }]
        
    });

     
    $('.casestudy-slider').slick();

    $('.ios-testimonial-slider').slick();
    

    $('.testimonial-slider').slick({
        dots: true,
        arrows: false
    });
    
    $('.homepg-slider').slick({
        arrows: true,
        autoplay: true,
        autoplaySpeed: 3000,
        dots: false
    });

     $('.portfolio-slider').slick({
        responsive: [
            {
                breakpoint: 991,
                settings: "unslick"
            }
        ]
    });

    $('.work-slider').slick({
      infinite: true,
      dots: true,
      autoplay: true,
      autoplaySpeed:4000,
      speed: 3000,
      pauseOnHover: false
    });


    //*****************************
    // Responsive Slider
    //*****************************

    var tabrespsliders = {
      1: {slider : '.difference-slider'},
      2: {slider : '.scroll-portfolio'},
      3: {slider : '.mobile-portfolio-slider'},
      4: {slider : ''},
      5: {slider : '.ios-service-box'},
      6: {slider : '.ios-slider1'}
    };


    $('.homepage-three-box').slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 2,
        dots: false,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 2000,
        responsive: [
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 535,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });



    //*****************************
    // Responsive Slider
    //*****************************
    var respsliders = {
      1: {slider : '.success-slider'},
      2: {slider : '.awards-slider'}, 
      3: {slider : '.experience-slider'},
      4: {slider : '.howitwork-slider'},
      5: {slider : '.deliverysteps-slider'},
      6: {slider : '.tab-slider'},
      7: {slider : '.package-slider'},
      8: {slider : '.seo-improve-slider'},
      9: {slider : '.two-block-slider'},
      10: {slider : '.tab-slider'},
      11: {slider : '.tabbing-slider'},
      12: {slider : '.aboutpg-slider'},
      13: {slider : '.aboutpg-qc-slider'},
      14: {slider : '.aboutpg-adv-slider'},
      15: {slider : '.assurance-slider'},
      16: {slider : '.aboutpg-asurance-slider'},
      17: {slider : '.augmented-apps-slider'},
      18: {slider : '.augmented-offering-left-slider'},
      19: {slider : '.augmented-offering-right-slider'},
      20: {slider : '.augmented-approach-slider'},
      21: {slider : '.augmented-industry-slider'},
      22: {slider : '.client-logo-slider'},
      23: {slider : '.mobile-app-platforms-slider'},
      24: {slider : '.homepage-left-side-slider'},
      25: {slider : '.homepage-box-slider'},
      26: {slider : '.career-box-slider'},
      27: {slider : '.career-openings-slider'},
      28: {slider : '.about-testimonials-slider'},
      29: {slider : '.mobile-apps-slider'},
      30: {slider : '.ourwork-slider'},
      31: {slider : '.services-slider'},
      32: {slider : '.for-mobile-work-slider'},
      33: {slider : '.client-list'}

    };

    //*****************************
    // Function for Responsive Slider 991
    //*****************************

    $.each(tabrespsliders, function() {

        $(this.slider).slick({
            arrows: false,
            dots: true,
            autoplay: true,
            settings: "unslick",
            responsive: [
                {
                  breakpoint: 2000,
                  settings: "unslick"
                },
                {
                    breakpoint: 991,
                    settings: {
                        unslick: true
                    }
                }
            ]
        });
    });


    //*****************************
    // Function for Responsive Slider 767
    //*****************************

    $.each(respsliders, function() {

        $(this.slider).slick({
            arrows: false,
            dots: true,
            autoplay: true,
            settings: "unslick",
            responsive: [
                {
                  breakpoint: 2000,
                  settings: "unslick"
                },
                {
                    breakpoint: 767,
                    settings: {
                        unslick: true
                    }
                }
            ]
        });
    });   
    
    $('.slider-tabs').slick({
        rows: 3,
        dots: true,
        arrows: true,
        infinite: true,
        draggable: false,
        fade: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        customPaging : function(slider, i) {
        var thumb = $(slider.$slides[i]).data();
        return '<a>'+(i+1)+'</a>';
        },
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesPerRow: 1,
                    rows: 2,
                    slidesToScroll: 1,
                    slidesToShow: 1,
                    dots: true
                }
            },
            {
                breakpoint: 480,
                settings: {
                    rows: 1,
                    slidesPerRow: 1,
                    slidesToScroll: 1,
                    slidesToShow: 1,
                    dots: true
                }
            }
        ]
    });
    
    $(".slider-tabs .slick-dots a").click(function() {
    $('html,body').animate({
        scrollTop: $(".toheading").offset().top},
        'slow');
    });
    
});


// $(document).ready(function(){

//     if(readCookie("cookie_accepted") == "1"){
//         $(".cookie-bar").hide();
//     }
//     else{
//         $(".cookie-bar").show();
//             $('.copyright').addClass('mbpx-45');
//             $('.cookie-btn').click(function(){
//                 $('.copyright').removeClass('mbpx-45');
//                 //$('.copyright').addClass('pbpx-10');

//             $('.cookie-bar').fadeOut();
//             createCookie("cookie_accepted", 1, 365);
//         });     
//     }

// });