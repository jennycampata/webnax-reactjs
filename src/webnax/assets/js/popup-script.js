import $ from 'jquery';

$(document).ready(function () {

    $('.modal').addClass('show-modal');


    $('.trigger').on('click',function () {
        console.log('testing');
        $('.modal').animate({marginLeft: '0px'}, 1000);
        $('.modal').addClass('show-modal');
        $('.modal').fadeIn("slow", function () {
            $(this).addClass("background");
        });
    });

   
    $('.close-button').on('click', function () {
        $('.modal').animate({marginLeft: '-1990px'}, 1000);
        $('.modal').removeClass("background");
    });
});
