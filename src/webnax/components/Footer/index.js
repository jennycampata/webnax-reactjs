import React, { Component } from "react";
// import "bootstrap/dist/css/bootstrap.min.css";

// Webnax JS
// import "../../webnax/js/jquery-1.11.1.js"
import "../../assets/js/popup-script.js";
// import "../../webnax/js/xlib.js"
// import "../../webnax/js/script.js"

export default class Footer3 extends Component {
    render() {
        return (
            <div>
                <div className="modal">
                    <div className="modal-content">
                        <span className="close-button">&times;</span>
                        <div className="main-div">
                            <section className="">
                                <div className="offset-1 req_frm_card">
                                    <div className="offset-1 req_frm ">
                                        <img className="req_frm_logo" src={require("../../assets/images/header-logo-small.png")} />
                                    </div>
                                    <div className="text-center mt-3 overlay_frm col-md-10 offset-md-1">
                                        <form id="cntctForm" className="validate-contct-frm col-md-12 custom-class"
                                            action="mailfunction.php"
                                            method="post"
                                            encType="application/">
                                            <div className="jform">
                                                <div className="row">
                                                    <div className="col-sm-6">
                                                        <div className="frmClm">
                                                            <label>Name <strong>*</strong></label>
                                                            <input type="text" name="name" className="required alphanumeric cname" required />
                                                        </div>
                                                    </div>
                                                    <div className="col-sm-6">
                                                        <div className="frmClm">
                                                            <label>Email <strong>*</strong></label>
                                                            <input type="email" name="email" className="required email cemail" required />
                                                        </div>
                                                    </div>
                                                    <div className="col-sm-6">
                                                        <div className="frmClm cle">
                                                            <label>Phone <strong>*</strong></label>
                                                            <div className="number-sec">
                                                                <input type="text" name="number" className="required number cphone"
                                                                    onKeyPress="return event.charCode >= 48 && event.charCode <= 57" required />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="col-sm-6">
                                                        <div className="frmClm">
                                                            <label>Type <strong>*</strong></label>
                                                            <select id="ddlType" name="type" className="ddlType overlay">
                                                                <option value="0">Type</option>
                                                                <option value="Individual">Individual</option>
                                                                <option value="Company">Company</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="frmClm">
                                                    <label>Services: </label>
                                                    <select multiple="multiple" id="ddlserviceCategory"
                                                        name="service[]">
                                                        <option value=" Mobile Applications ">Mobile Applications</option>
                                                        <option value=" Logo Desiging ">Logo Desiging</option>
                                                        <option value=" SEO Services ">SEO Services</option>
                                                        <option value=" Pay Per Click ">Pay Per Click</option>
                                                        <option value=" Social Media ">Social Media</option>
                                                    </select>
                                                </div>
                                                <div className="frmClm">
                                                    <label>PROJECT DETAILS: <strong>*</strong></label>
                                                    <textarea name="msg" className="required cmessage"></textarea>
                                                </div>
                                                <div className="frmClm">
                                                    <input type="submit" name="" value="" />
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>

                <footer className="pf bg-secondary sec-padding --small hidden-md-down" style={{ paddingBottom: "0px" }}>
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-4 col-md-6 border">
                                <div className="footer-logo d-inline-block">
                                    <a href="index">
                                        <img src={require("../../assets/images/footer-logo.png")} alt="*" />
                                    </a>
                                </div>
                                <div className="footer-contact">
                                    <p className=" fw-normal fc-spgrey d-block pl-7 pbpx-10 " data-content="Call Us 807-788-4037"><i
                                        className="fa fa-phone"></i></p>
                                    <p className="address fw-normal fc-spgrey w-68 pl-7 pbpx-20 "
                                        data-content="140 Carlton street Toronto, ON M5A3W7"><i className="wicon icon-location"></i></p>
                                </div>
                                {/* <!-- wicon icon-location --> */}
                            </div>
                            <div className="col-lg-3 col-md-6 col-sm-6 hide-tab">
                                <nav className="fn">
                                    <p className="--title fc-grey fs-medium fw-semi-bold mbpx-14"><a href="#">Blog</a></p>
                                    {/* <!-- <ul className="unstyled --arrow-list">
                         <li><a href="apps"> Mobile Applications</a></li>
                          <li><a href="logo"> Logo Desiging</a></li>
                        <li><a href="seo"> SEO Services</a></li>
                        <li><a href="ppc">Pay Per Click</a></li>
                        <li><a href="social">Social Media</a></li>
                    </ul> --> */}
                                </nav>
                            </div>
                            <div className="col-lg-2 col-md-6 col-sm-6 hide-tab"></div>
                            <div className="col-lg-3 col-md-6 col-sm-12 hide-tab">
                                <nav className="fn">
                                    <p className="--title fc-grey fs-medium fw-semi-bold mbpx-14">About</p>
                                    <ul className="unstyled --arrow-list">
                                        <li><a href="the-company/">The Company</a></li>
                                        <li><a href="contact-us/">Contact Us</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </footer>


                <footer className="copyright ptpx-18 pbpx-18 bg-dkgrey text-md-center">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-12 hidden-lg-up">
                                <a href="index">
                                    <img src={require("../../assets/images/footer-logo.png")} className="text-md-center m-auto mbpx-10" alt="*" />
                                </a>
                            </div>
                            <div className="col-lg-4 col-md-4">
                                <div className="text-lg-center text-xs-left">
                                    <p className="fs-small fc-grey m-auto ptpx-2">&copy;
                                        <script>{document.write(new Date().getFullYear())}</script>
                                        <b>WebNax</b><sup style={{ fontSize: "7px" }} >TM</sup> Inc. All rights reserved.
                                    </p>
                                </div>
                            </div>
                            <div className="col-lg-4 col-md-4"></div>
                            <div className="col-lg-4 col-md-4">
                                <div className="text-lg-center social-links ta-right">
                                    <a href="#" target="_blank"><i className="icon-facebook xicon facebook fab fa-facebook"></i></a>
                                    <a href="#" target="_blank"><i className="icon-twitter xicon twitter fab fa-twitter"></i></a>
                                    <a href="#" target="_blank" className="insta-icon insta"><i
                                        className="icon-instagram xicon insta fab fa-instagram"></i></a>
                                    <a href="#" target="_blank" className=""><i
                                        className="icon-linkedin xicon linkedin fab fa-linkedin"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>

                <div className="mobile-cta">
                    <a href="sms:8077884037" className="xicon text">
                        <i className="icon-chat"></i>
                    </a>
                    <a href="tel:8077884037" className="xicon call">
                        <i className="icon-call"></i>
                    </a>
                </div>

            </div>
        );
    }
}


