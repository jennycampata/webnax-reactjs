import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
// Webnax
import "../../assets/css/style.css";

export default class Navbar3 extends Component {

    // handleNavigation = (e) => {
    //     this.props.history.push(e)
    //     // console.log(this.props)
    // }

    render() {
        return (
            <div>
                <div className="mobile-nav">
                    <a href="index" className="callnow"><img src={require("../../assets/images/header-logo-small.png")} alt="*" /></a>
                    <nav>
                        <ul className="unstyled mainnav">
                            <li><a href="index">Home</a></li>
                            <li><a href="#">Work</a></li>
                            <li><a href="#">Services <i className="xicon icon-arrow_down"></i></a>
                                <ul className="firstlevel unstyled">
                                    <li><a href="mobile-applications"> Mobile Applications</a></li>
                                    <li><a href="logo-design"> Logo Desiging</a></li>
                                    <li><a href="social-media-marketing">Social Media</a></li>
                                    <li><a href="search-engine-optimization"> SEO Services </a></li>
                                    <li><a href="pay-per-click">Pay Per Click</a></li>
                                </ul>
                            </li>
                            <li><a href="#">About <i className="xicon icon-arrow_down"></i></a>
                                <ul className="firstlevel unstyled">
                                    <li><a href="about-us">The Company</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Blog</a></li>
                            <li><a href="contact-us">Contact Us</a></li>
                            <li>
                                <a href="request_form.php" className="free-estimate tt-uppercase">Request Info</a>
                            </li>
                        </ul>
                    </nav>
                </div>
                
                <main className="app-container">
                    <div className="mobile-nav-btn">
                        <span className="lines"></span>
                    </div>
                    <header className="phnew clearfix">
                        <div className="col-md-2 ">
                            <a href="index" className="logo d-inline-block">
                                <div className="red logo-resize-red">
                                    <img src={require("../../assets/images/logo.png")} alt="" />
                                </div>
                                <div className="white logo-resize-white">
                                    <img src={require("../../assets/images/logo.png")} alt="" />
                                </div>
                            </a>
                        </div>
                        <div className="col-lg-7 col-md-6 col-nopadd hidden-md-down">
                            <nav className="pnew mtpx-26">
                                <ul className="unstyled inline">
                                    <li className="#"><a href="#">Work</a></li>
                                    <li className="dropdown "><a href="#">Services </a>
                                        <ul className="firstlevel unstyled">
                                            <li><a href="mobile-applications"> Mobile Applications</a></li>
                                            <li><a href="logo-design"> Logo Desiging</a></li>
                                            <li><a href="social-media-marketing">Social Media</a></li>
                                            <li><a href="search-engine-optimization"> SEO Services </a></li>
                                            <li><a href="pay-per-click">Pay Per Click</a></li>
                                        </ul>
                                    </li>
                                    <li className="dropdown "><a href="">about </a>
                                        <ul>
                                            <li><a href="about-us">The Company</a></li>
                                        </ul>
                                    </li>
                                    <li className="#"><a href="contact-us">contact</a></li>
                                </ul>

                            </nav>
                        </div>
                        <div className="col-lg-3 col-md-4 col-nopadd hidden-md-down">
                            <div className="nav-aside ta-right">
                                <span className="phone"><a href="tel:416-553-4219" className="icon icon-call"> 807-788-4037 </a></span>
                                <a href="#" className="trigger free-estimate tt-uppercase">Request Info</a>
                            </div>
                        </div>
                    </header>
                </main>
            
            </div>
        );
    }
}


