import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../assets/css/popup-style.css";
import "../../assets/css/app.css";

export default class SecNavbar extends Component {

    render() {
        return (
            <div>
                <a href="#" className=" request-sidebtn">
                    <button className="trigger sidebtn"><img src={require("../../assets/images/new-request-sidebtn.png")} alt="*" /></button>
                </a>

                <div className="mobile-nav">
                    <a href="index" className="callnow"><img src={require("../../assets/images/header-logo-small.png")} alt="*" /></a>
                    <nav>
                        <ul className="unstyled mainnav">
                            <li><a href="index">Home</a></li>
                            <li><a href="#">Work</a></li>
                            <li><a href="#">Services <i className="xicon icon-arrow_down"></i></a>
                                <ul className="firstlevel unstyled">
                                    <li><a href="mobile-applications"> Mobile Applications</a></li>
                                    <li><a href="logo-design"> Logo Desiging</a></li>
                                    <li><a href="social-media-marketing">Social Media</a></li>
                                    <li><a href="search-engine-optimization"> SEO Services </a></li>
                                    <li><a href="pay-per-click">Pay Per Click</a></li>
                                </ul>
                            </li>
                            <li><a href="#">About <i className="xicon icon-arrow_down"></i></a>
                                <ul className="firstlevel unstyled">
                                    <li><a href="about-us">The Company</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Blog</a></li>
                            <li><a href="contact-us">Contact Us</a></li>
                            <li><a href="request_form.php" className="">Request Info</a></li>
                        </ul>
                    </nav>
                </div>

                <main className="app-container">
                    <div className="mobile-nav-btn">
                        <span className="lines"></span>
                    </div>
                    {/* <!-- Header  --> */}
                    <header className="ph clearfix">
                        <div className="col-lg-2">
                            <a href="index" className="headlogo">
                                <img src={require("../../assets/images/logo.png")} className="sm-logo" alt="*" />
                            </a>
                        </div>
                        <div className="col-lg-10 col-nopadd hidden-md-down">
                            <nav className="pn float-right clearfix">
                                <ul className="unstyled inline ta-right nomar main-nav mtpx-20 float-left">
                                    <li><a href="about-us">About</a></li>
                                    <li><a href="mobile-applications"> Mobile Applications</a></li>
                                    <li><a href="logo-design"> Logo Desiging</a></li>
                                    <li><a href="social-media-marketing">Social Media</a></li>
                                    <li><a href="search-engine-optimization"> SEO Services </a></li>
                                    <li><a href="pay-per-click">Pay Per Click</a></li>
                                    <li><a href="contact-us">Contact</a></li>
                                </ul>
                                <a href="#" className="trigger btn-request-info float-right">Request Info</a>
                            </nav>
                        </div>
                    </header>
                </main>
            </div>
        );
    }
}


