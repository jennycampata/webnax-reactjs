import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";

// Webnax CSS
import "../../assets/css/popup-style.css";

// Component
import Navbar from "../../components/Navbar";
import Footer from "../../components/Footer";

export default class Home extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div>
                <Navbar />

                <div id="ads_modal_outer">
                    <div className="ads_modal">
                        <div className="sideImg"></div>
                        <div className="textContent">
                            <h6 className="fs-h2">We are offering 10% discount on our <br /><span className="ff-secondary fc-lgrey">all services</span> for
                    a limited time.</h6>
                            <form action="promotion_mailfunction.php" method="post">
                                <input type="email" placeholder="Your Email" />
                                <input type="submit" value="Get Discount" />
                            </form>
                        </div>
                    </div>
                </div>

                <div className="box" id="box">
                    <button className="trigger request-sidebtn"><img src={require("../../assets/images/new-request-sidebtn.png")} alt="*" /></button>
                </div>

                <section className="wooslider">
                    <section className="hero-banner homepage">
                        <section className="videobanner d-table w-100">
                            <div className="d-table-cell va-middle">
                                <div className="container">
                                    <div className="banner-content ">
                                        <h6 className="vidtext fc-white mbpx-20 tt-uppercase lh-normal fs-h1 fs-m-h3">A One-Stop Solution <br /> to
                                Cater Your <br />Marketing Needs</h6>

                                        <a href="#" className="bttn bttnOutline">VIEW OUR WORK</a>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <img src={require("../../assets/images/poster-3.png")} className="poster-image" />
                        <video className="mainvideo" controls autoPlay loop muted playsInline preload="metadata">
                            <source src={require("../../assets/images/movie.mp4")} type="video/mp4" />
                            Your browser does not support the video tag.
                        </video>
                        <div className="video-overlay"></div>
                    </section>
                    <section className="hero-banner homepage"
                        style={{
                            backgroundImage: `url(${require('../../assets/images/slider1.jpg')})`,
                            display: 'table',
                            backgroundSize: "cover",
                            backgroundPosition: "24% top"
                        }}
                    >
                        <div className="d-table-cell va-middle">
                            <div className="container">
                                <div className="row">
                                    <div className="slide-text">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section className="hero-banner homepage"
                        style={{
                            backgroundImage: `url(${require('../../assets/images/slider5.jpg')})`,
                            display: 'table',
                            backgroundSize: "cover",
                            backgroundPosition: "center top"
                        }}
                    >
                        <div className="d-table-cell va-middle">
                            <div className="container">
                                <div className="row">
                                    <div className="slide-text">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                    <section className="hero-banner homepage"
                        style={{
                            backgroundImage: `url(${require('../../assets/images/slider3.jpg')})`,
                            display: 'table',
                            backgroundSize: "cover",
                            backgroundPosition: "center top"
                        }}
                    >
                        <div className="d-table-cell va-middle">
                            <div className="container">
                                <div className="row">
                                    <div className="slide-text">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section className="hero-banner homepage"
                        style={{
                            backgroundImage: `url(${require('../../assets/images/slider4.jpg')})`,
                            display: 'table',
                            backgroundSize: "cover",
                            backgroundPosition: "center top"
                        }}
                    >
                        <div className="d-table-cell va-middle">
                            <div className="container">
                                <div className="row">
                                    <div className="slide-text">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                </section>


                <div className="bg-white sec-padding sec-overlap">
                    <div className="container ">
                        <h6 className="ta-center lh-medium fs-h2 fs-m-h3"> Services <span className="ff-secondary fc-lgrey">We Offer</span></h6>
                        <p className="ta-center fs-medium">
                            We’re a creative digital agency, focusing on connecting customers with companies through ground-breaking
                            digital marketing solutions.</p>
                        <div className="row mtpx-50">
                            <div className="col-md-12">
                                <div className="box --home-box-side clearfix">
                                    <div className="unstyled inline homepage-box-slider grid-block --type-mobile-full clearfix">
                                        <a href="mobile-applications"
                                            data-img={`url(${require('../../assets/images/mobile-application.jpg')})`}
                                            className="inner-content">
                                            <p className="name lh-medium"> Mobile<br />Applications </p>
                                        </a>
                                        <a href="logo-design"
                                            data-img={`url(${require('../../assets/images/logo.jpg')})`}
                                            className="matchheight inner-content">
                                            <p className="name lh-medium fc-white"> Logo<br />Desiging</p>
                                        </a>
                                        <a href="search-engine-optimization"
                                            data-img={`url(${require('../../assets/images/digitalmarketing.jpg')})`}
                                            className="matchheight inner-content">
                                            <p className="name lh-medium fc-white"> SEO<br />Services </p>
                                        </a>
                                        <a href="social-media-markarting"
                                            data-img={`url(${require('../../assets/images/smm.jpg')})`}
                                            className="matchheight inner-content">
                                            <p className="name lh-medium fc-white"> Social Media<br />Marketing </p>
                                        </a>
                                        <a href="pay-per-click"
                                            data-img={`url(${require('../../assets/images/ppc.jpg')})`}
                                            className="matchheight inner-content">
                                            <p className="name lh-medium fc-white"> Pay Per Click </p>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div >

                    </div >
                </div >

                <div className="bg-white sec-padding bg_img--overlap">
                    <div className="container">
                        <h1 className="ls-xsmall title ta-center mbpx-30 w-80 m-auto fs-h2 fs-m-h3">Build & Design A Website
                            <span className="ff-secondary fc-lgrey">That Represents </span>
                            the Professionalism <span className="fc-primary"> of Business</span>
                        </h1>
                        <p className="ta-center fs-medium fs-17px"> Having an experience of more than a decade in our hands, we build a
                        website according to the modern needs of a business. Be it a small startup or a well established enterprise, an
                        online existence is a must to survive in the digital world. <b>WebNax</b> is a one stop shop for web design
                                                and development solutions. We propose a well planned strategic process to get your imagination into real
                        by understanding your business structure and <strong>design a professional website</strong> accordingly.
                        WebNax is the <b>leading IT firm based in Canada.</b> Ever since the introduction of our organization in
                                                2017, we have achieved a great success and served more than 500 clients providing them the best solution for
                        <strong>web designing and development</strong> as per their requirements.</p>
                        <div className="row mtpx-50">

                            <div className="col-md-6">
                                <div className="box --home-box-side clearfix">
                                    <div className="unstyled inline grid-block --type-mobile-full clearfix">
                                        <h2 className="fs-h3 fs-m-h4">What Puts You In The Right Hand</h2>
                                        <p className="homecontent fs-15px">
                                            With our highly skilled developers you get into the right hands, and we ensure that you
                                            keep putting your interest from the scratch to hatch because customer satisfaction is
                                            the key factor that is our extremely focused aim. With years of experience, we have a
                                            dedicated team of excelled web developers to align with you, communicate and understand
                                            your requirements. Proposing you the best possible solutions, comply the instructions and
                                            taking it into the action. Thus, you request and we make it for you. </p>

                                    </div>
                                </div>
                            </div>


                            <div className="col-md-6">
                                <div className="box --home-box-side clearfix">

                                    <div className="unstyled inline  grid-block --type-mobile-full clearfix">
                                        <h2 className="fs-h3 fs-m-h4">Get A Professional Website Design</h2>

                                        <p className="homecontent fs-15px">
                                            In today's market place having a reputation is essential along with this you need an
                                            online representation of your business. With a professional web design you put an online
                                            image of your business. That's where <b>WebNax</b> helps you beyond the limit. We propose
                                            you multiple web designs that perfectly fits in to your business industry. Not only that,
                                            in the domain of web development we cater almost every platform be it eCommer, Magento,
                                            OpenCart, Shopify or its a custom website development. </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <section className="homgpg-two-box">
                    <div className="grid-block --type-two-blocks --type-mobile-full clearfix">
                        <div
                            style={{ backgroundImage: `url(${require('../../assets/images/marketing-expertise.jpg')})`, }}
                            className="res-pad pl-10 pr-5 ptpx-60 pbpx-60 matchheight bg-cover br-right text-sm-center">
                            <h3 className="fc-white fw-normal mbpx-10 red fs-h4 fs-m-h4"> We Convert Imaginations Into Reality. </h3>
                            <p className="fc-white"><b>WebNax</b> helps startups to enterprises building their identity. </p>
                            {/* <!-- <a href="team/index" className="btn btn-primary mtpx-30">MEET OUR TEAM</a> --> */}
                        </div>
                        <div
                            style={{ backgroundImage: `url(${require('../../assets/images/careers.jpg')})`, }}
                            className="res-pad pl-5 pr-10 ptpx-60 pbpx-60 matchheight bg-cover  text-sm-center">
                            <h3 className="fc-white fw-normal mbpx-10 fs-h4 fs-m-h4">Business Relationship</h3>
                            <p className="fc-white">We believe in creating a long-term business relation with our clients and stick to them on their path of growth.</p>
                            {/* <!-- <a href="team/index" className="btn btn-primary mtpx-30">LEARN MORE</a> --> */}
                        </div>
                    </div>
                </section>


                <section className="bottom-contact bg-primary sec-padding --small">
                    <div className="container">
                        <h3 className="fc-white ta-center fw-normal lh-medium mb-4 head-title hidden-sm-down">Let's Begin With Us! </h3>
                        <div className="w-80 m-auto clearfix">
                            <div className="col-md-4 ta-center">
                                <a href="#" className="d-inline-block ta-center" onClick="setButtonURL();">
                                    <i className="xicon icon-chat h2 fc-white mb-8 d-block float-md-none float-xs-left fas fa-handshake"></i>
                                    <span className="h4 fc-white lh-normal ">Chat Live</span>
                                    <div className="fs-large fc-white hidden-sm-down">to discuss your project</div>
                                </a>
                            </div>
                            <div className="col-md-4 ta-center ">
                                <a href="#" className="trigger btn bg-black mbpx-40 mtpx-40 h5 fc-white fw-normal --large ">Request Info</a>
                                <a href="request_form.php"
                                    className="mobile-btn btn bg-black mbpx-40 mtpx-40 h5 fc-white fw-normal --large ">Request Info</a>

                            </div>
                            <div className="col-md-4 ta-center">
                                <a className="d-inline-block ta-center" href="tel:807-788-4037">
                                    <i className="xicon icon-call h2 fc-white mb-8 d-block float-md-none float-xs-left"></i>
                                    <span className="h4 fc-white lh-normal"> 807-788-4037 </span>
                                    <div className=" fs-large fc-white hidden-sm-down">Call us anytime</div>
                                </a>
                            </div>
                        </div>
                    </div>
                </section>

                <Footer />

            </div >
        );
    }
}


