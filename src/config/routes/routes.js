import React from 'react';
// import { Router, Route } from 'react-router-dom';
// import { createBrowserHistory } from 'history';

import AdminRoutes from "./adminRoutes";
import UserRoutes from "./userRoutes";
import WebnaxDemo from "./WebnaxDemo";

// Routes For Navigation
const MyRoutes = () => (
    <div>
        <AdminRoutes />
        <UserRoutes />
        <WebnaxDemo />
    </div>
)

export default MyRoutes